from django.db import models
from .shared.util.constant import Constant

"""Provide model for count information of words on website
:param models.Model: Inheritance for the Model class of Django Framework
"""
class CountInfo(models.Model):
  word = models.CharField(null=True, blank=True, max_length=Constant.WORD_MAX_LENGTH_INT)
  url = models.URLField(null=True, blank=True)
  count = models.IntegerField()

  """Return the class name which consists of word with the url of website
  :param self: Reference for word and url variables of the instance
  :return self.word + Constant.AT_STRING + self.url: Composed text value
  """
  def __str__(self):
    return self.word + Constant.AT_STRING + self.url
