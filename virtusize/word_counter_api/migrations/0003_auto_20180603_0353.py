# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-06-03 03:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('word_counter_api', '0002_auto_20180602_0647'),
    ]

    operations = [
        migrations.CreateModel(
            name='CountInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('word', models.CharField(max_length=10)),
                ('url', models.URLField()),
                ('count', models.IntegerField()),
            ],
        ),
        migrations.DeleteModel(
            name='CountResult',
        ),
    ]
