import urllib.request
from socket import timeout
from .constant import Constant

"""Get HTML based on the url and return the result.
"""
class Curl:
  """Initialize by accepting a targeted url.
  :param self: Self variable to set the url member variable in the instance.
  :param url: Url value to set the member.
  """
  def __init__(self, url):
    self._url = url;

  """Get HTML based on the url.
  :param self: Used for registering as the member function of the instance.
  """
  def get_html(self):
    result_dict = {}

    response = urllib.request.urlopen(self._url, timeout=Constant.TIME_OUT_INT)

    html_string = response.read().decode(Constant.UTF8_STRING)
    response.close()
    result_dict[Constant.RESULT_CODE_STRING] = Constant.SUCCESS_CODE_INT
    result_dict[Constant.RESULT_MESSAGE_STRING] = Constant.SUCCESS_ON_CURL_STRING
    result_dict[Constant.RESULT_VALUE_STRING] = html_string
    
    return result_dict
