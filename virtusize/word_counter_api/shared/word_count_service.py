from .word_count_html_parser import WordCountHtmlParser
from .util.constant import Constant
from .util.curl import Curl

"""Provide HTML from curl as well as the count of word from WordCountHtmlParser.
"""
class WordCountService:
  """Get Html from curl.
  :param self: Used for registering as the member function of the instance.
  :param url: An url value for curl to get HTML.
  :return get_html(): HTML string from curl.
  """
  def get_html_from_curl(self, url):
    curl = Curl(url)
    return curl.get_html()

  """Get the count of word from WordCountHtmlParser.
  :param self: Used for registering as the member function of the instance.
  :param targetWord: Target word to be counted in parsing HTML.
  :param targetHtml: Target HTML which was from curl.
  :return wordCountHtmlParser.get_word_count(): Calculated the count of word in HTML.
  """
  def get_word_count_from_html(self, targetWord, targetHtml):
    wordCountHtmlParser = WordCountHtmlParser(targetWord)
    wordCountHtmlParser.feed(targetHtml)
    return wordCountHtmlParser.get_word_count()
