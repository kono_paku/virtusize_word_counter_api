from rest_framework import serializers
from ..models import CountInfo
from .util.constant import Constant

"""Serializer for request validation 
"""
class CountRequestSerializer(serializers.ModelSerializer):
  """Meta information used to serialize the data 
  """
  class Meta:
    model = CountInfo
    fields = (
        Constant.WORD_STRING,
        Constant.URL_STRING,
    )


"""Serializer for response data 
"""
class CountResponseSerializer(serializers.ModelSerializer):
  """Meta information used to serialize the data 
  """
  class Meta:
    model = CountInfo
    fields = (
      Constant.WORD_STRING,
      Constant.URL_STRING,
      Constant.COUNT_STRING
    )
