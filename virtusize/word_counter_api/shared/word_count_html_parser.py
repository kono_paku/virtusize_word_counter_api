import re
from html.parser import HTMLParser
from .util.constant import Constant

"""Parse HTML from curl as well as calculate word counts in HTML
:param models.HTMLParser: Inheritance for the HTMLParser class of python library
"""
class WordCountHtmlParser(HTMLParser):
  """Initialize by accepting a targeted url.
  :param self: Self variable to set the url member variable in the instance.
  :param target_word: Targeted word value to set the member variable.
  """
  def __init__(self, target_word):
    HTMLParser.__init__(self)
    self._target_word = target_word
    self._word_count = 0;
    self._target_tag = False

  """Check whether start tag is the targeted one.
  :param self: Used for registering as the member function of the instance.
  :param tag: A parsed tag value in HTML.
  :param attrs: An attribute values of tag from python library, but will not be used.
  """
  def handle_starttag(self, tag, attrs):
    # Check text in tag only for target tags.
    if tag in Constant.NOT_PARSING_TAG_SET:
      self._target_tag = False
    else:
      self._target_tag = True

  """Check targeted word is in the sentence of tags, if the tag is the targeted one, 
     And if the targeted word exists, increase the count of word.
  :return : Skip if a tag is not the targeted one.
  """
  def handle_data(self, parsed_data):
    # Skip checking process if tag is not the target one.
    if self._target_tag != True:
      return

    # Convert a string of text in sentence format into list.
    parsed_data_list = parsed_data.split()

    # Check each word in list.
    for parsed_data_element in parsed_data_list:
      # Compare target word and each word in list by changing lower character.
      # If it is the same word, the variable for word count will be increased.
      if self._target_word.lower() == re.sub(
        Constant.NOT_WORD_DIGITS_REGEX_STRING,
        Constant.EMPTY_STRING,
        parsed_data_element
      ).lower():
        self._word_count += Constant.NUMBER_ONE_INT

  """Get the count of word as a result of parsing HTML
  :param self: Used for registering as the member function of the instance.
  :return self._word_count: Calculated word count value.
  """
  def get_word_count(self):
    return self._word_count;
