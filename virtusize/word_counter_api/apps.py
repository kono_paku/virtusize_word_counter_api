from django.apps import AppConfig


class WordCounterApiConfig(AppConfig):
    name = 'word_counter_api'
