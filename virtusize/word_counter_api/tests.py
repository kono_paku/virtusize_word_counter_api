from django.test import TestCase
from unittest.mock import patch, MagicMock
from django.db.utils import IntegrityError
from django.test import Client
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction

from .models import CountInfo

class VirtusizeWordCounterApiTestCase(TestCase):

    def setUp(self):
        self._count_info = CountInfo.objects.create(
            word='fit',
            url='http://www.virtusize.jp',
            count=5
        )

    def test_count_info_word(self):
      self.assertEqual(
        self._count_info.word,
        'fit'
      )

    def test_count_info_url(self):
      self.assertEqual(
        self._count_info.url,
        'http://www.virtusize.jp'
      )

    def test_count_info_count(self):
      self.assertEqual(
        self._count_info.count,
        5
      )

    def test_count_info_str(self):
      self.assertEqual(
        str(self._count_info),
        'fit at http://www.virtusize.jp'
      )

    def test_count_info_field_null_validation_url(self):
      with self.assertRaises(IntegrityError):
        with transaction.atomic():
          count_info = CountInfo.objects.create(
           word='fit',
           count=5
          )

    def test_count_info_field_null_validation_word(self):
      with self.assertRaises(IntegrityError):
        with transaction.atomic():
          count_info = CountInfo.objects.create(
            url='http://www.virtusize.jp',
            count=5
          )

    def test_count_info_field_null_validation_count(self):
      with self.assertRaises(IntegrityError):
        with transaction.atomic():
          count_info = CountInfo.objects.create(
            word='fit',
            url='http://www.virtusize.jp'
          )

    def test_count_info_field_null_validation_all(self):
      with self.assertRaises(IntegrityError):
        with transaction.atomic():
          count_info = CountInfo.objects.create()

    # def setUp(self):
    #   client = Client()

    # def test_count_info_word(self):
    #   response = client.get(reverse('word_count'))
    #   self.assertEqual(response.status_code,response)

    # @patch('example.models.request')
    # def test_get_page(self, req):
    #     url = MagicMock()
    #     self.example._get_page(url)
    #     req.assert_called_once_with('GET', url)

    # @patch('example.models.Example._get_page')
    # def test_name_in_page_calls_get_page(self, getpage):
    #     url = MagicMock()
    #     self.example.name_in_page(url)
    #     getpage.assert_called_once_with(url)

    # @patch('example.models.Example._get_page')
    # def test_name_in_page(self, getpage):
    #     getpage.return_value = MagicMock(
    #         content='Text that contains {} in it'.format(self.example.name))
    #     self.assertTrue(self.example.name_in_page(MagicMock()))

    # @patch('example.models.Example._get_page')
    # def test_name_in_page_not(self, getpage):
    #     getpage.return_value = MagicMock(
    #         content='Text that does not contain the name')
    #     self.assertFalse(self.example.name_in_page(MagicMock()))

    # @patch('example.models.Example._get_page')
    # def test_name_in_page_closes_response(self, getpage):
    #     resp = MagicMock()
    #     getpage.return_value = resp
    #     self.example.name_in_page(MagicMock())
    #     resp.close.assert_called_once_with()
