# impot from python
import json

# import from Django framework
from django.shortcuts import get_object_or_404
from django.core.exceptions import ValidationError
import logging

# import from Django rest framework
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework import status

# import from custom source files
from .models import CountInfo
from .shared.count_serializers import CountRequestSerializer, CountResponseSerializer
from .shared.word_count_service import WordCountService
from .shared.util.constant import Constant

logger = logging.getLogger(__name__)

"""Return the response for the count of word in the website.
:param request: Request information for finding out the word count.
:return Response: Resoponse information which includes the result of word count or error.
"""
# Supported the message of not allowed methods
@api_view(
  [
    Constant.GET_METHOD_STRING,
    Constant.POST_METHOD_STRING,
    Constant.PUT_METHOD_STRING,
    Constant.DELETE_METHOD_STRING,
    Constant.PATCH_METHOD_STRING,
    Constant.HEAD_METHOD_STRING,
    Constant.CONNECT_METHOD_STRING,
    Constant.OPTIONS_METHOD_STRING,
    Constant.TRACE_METHOD_STRING
    ]
  )
def word_count(request):
  # Initialize the response value with a empty dictionary.
  response_dict = {}

  # Return an response for method not allowed.
  if (request.method != Constant.POST_METHOD_STRING):
    status_code = status.HTTP_405_METHOD_NOT_ALLOWED
    response_dict.update(
        {
            Constant.RESULT_CODE_STRING: status_code,
            Constant.RESULT_MESSAGE_STRING: Constant.HTTP_METHOD_NOT_ALLOWED_STATUS_MESSAGE_STRING,
            Constant.RESULT_VALUE_STRING: Constant.HTTP_SUPPORTED_METHOD_ERROR_MESSAGE_STRING
        }
    )
    return Response(response_dict, status_code)
    
  # Check content_type whether application/json is.
  if request.content_type != Constant.JSON_CONTENT_TYPE_STRING:
    status_code = status.HTTP_400_BAD_REQUEST
    logger.error(Constant.HTTP_CONTENT_TYPE_ERROR_MESSAGE_STRING)
    response_dict.update(
      {
        Constant.RESULT_CODE_STRING: status_code,
        Constant.RESULT_MESSAGE_STRING: Constant.HTTP_BAD_REQUEST_STATUS_MESSAGE_STRING,
        Constant.RESULT_VALUE_STRING: Constant.HTTP_CONTENT_TYPE_ERROR_MESSAGE_STRING
      }
    )
    return Response(response_dict, status = status_code)

  # Convert json byte in to dictionary.
  json_request = json.loads(request.body)

  # Input each json value into CountRequestSerializer for validation.
  count_request_serializer = CountRequestSerializer(
    data = {
      Constant.WORD_STRING: json_request.get(Constant.WORD_STRING),
      Constant.URL_STRING: json_request.get(Constant.URL_STRING),
    }
  )

  # Check whether the request parameters are valid.
  if not count_request_serializer.is_valid():
    status_code = status.HTTP_400_BAD_REQUEST
    logger.error(count_request_serializer.errors)
    response_dict.update(
      {
        Constant.RESULT_CODE_STRING: status_code,
        Constant.RESULT_MESSAGE_STRING: Constant.HTTP_BAD_REQUEST_STATUS_MESSAGE_STRING,
        Constant.RESULT_VALUE_STRING: count_request_serializer.errors
      }
    )
    return Response(response_dict, status=status_code)
    
  # Query the model of CountInfo in order to check whether there was the same request.
  count_info = CountInfo.objects.filter(
    word = count_request_serializer.data[Constant.WORD_STRING],
    url = count_request_serializer.data[Constant.URL_STRING]
  ).first()

  # Set up a serializer for response if the same request exists
  if count_info:
    count_response_serializer = CountResponseSerializer(
        data={
            Constant.WORD_STRING: count_info.word,
            Constant.URL_STRING: count_info.url,
            Constant.COUNT_STRING: count_info.count,
        }
    )
  # Create WordCountService to make curl request for obtaining HTML and parse the HTML
  else:
    word_count_service = WordCountService()
    # Get targeted HTML by calling a curl request.
    result_dict = word_count_service.get_html_from_curl(count_request_serializer.data[Constant.URL_STRING])
    
    # Return an error page if the curl request fails.
    if result_dict.get(Constant.RESULT_CODE_STRING) != Constant.SUCCESS_CODE_INT:
      status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
      logger.error(count_request_serializer.errors)
      response_dict.update(
        {
          Constant.RESULT_CODE_STRING: status_code,
          Constant.RESULT_MESSAGE_STRING: Constant.HTTP_INTERNAL_SERVER_ERROR_STATUS_MESSAGE_STRING,
          Constant.RESULT_VALUE_STRING: count_request_serializer.errors
        }
      )
      return Response(response_dict, status=status_code)

    # Start to parse and get a word count as a result.
    word_count = word_count_service.get_word_count_from_html(
      count_request_serializer.data[Constant.WORD_STRING],
      result_dict.get(Constant.RESULT_VALUE_STRING)
    )
    # Set up a serializer for validation of request parameters.
    count_response_serializer = CountResponseSerializer(
      data = {
            Constant.WORD_STRING: count_request_serializer.data[Constant.WORD_STRING],
            Constant.URL_STRING: count_request_serializer.data[Constant.URL_STRING],
            Constant.COUNT_STRING: word_count,
        }
    )
    
  # Check whether response data is valid.
  if not count_response_serializer.is_valid():
    status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    logger.error(count_response_serializer.errors)
    response_dict.update(
      {
        Constant.RESULT_CODE_STRING: status_code,
        Constant.RESULT_MESSAGE_STRING: Constant.HTTP_INTERNAL_SERVER_ERROR_STATUS_MESSAGE_STRING,
        Constant.RESULT_VALUE_STRING: count_response_serializer.errors
      }
    )
    return Response(response_dict, status_code)

  # Save the result of word count information into Database in case there is no any the same one.
  if not count_info:
    count_response_serializer.save()

  # Return an response of error if the validation fails.
  status_code = status.HTTP_200_OK
  response_dict.update(
    {
      Constant.RESULT_CODE_STRING: status_code,
      Constant.RESULT_MESSAGE_STRING: Constant.HTTP_OK_STATUS_MESSAGE_STRING,
      Constant.RESULT_VALUE_STRING: count_response_serializer.data
    }
  )
  return Response(response_dict, status_code)
