#Word Counter API
----
  This endpoint explains Word Counter API.

##HTTP Request
`POST http://ec2-18-216-85-93.us-east-2.compute.amazonaws.com:8000/wordcount`
    

##Request Format
**Content-Type :**

`application/json`

**Parameters :**

Name          | Type           | Description                    
------------- | -------------- |------------------------------ 
`word`        | string         | **Required** The targeting word to count. The maximum length is 20.   
`url`         | string         | **Required** The targeting url of website to count. The maximum length is 200.     

**Example :**

    {
      "word" : "fit",
      "url":"http://www.virtusize.jp"	
    }

##Response Format
  
**Content-Type : **

`application/json`
   
**Success Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Result value for counting the word in website. Include `word`, `url`, `count`.


**Example:**

    {
        "result_code": 200,
        "result_message": "OK",
        "result_value": {
          "word": "fit",
          "url": "http://www.virtusize.jp",
          "count": 5
        }
    }

**Error Content : **

Name                | Type           | Description                    
------------------- | -------------- |------------------------------ 
`result_code`       | integer        | HTTP status code
`result_message`    | string         | HTTP status message
`result_value`      | json           | Error message in detail

**Example:**
    
    {
        "result_code": 405,
        "result_message": "Method Not Allowed",
        "result_value": "POST method is only supported."
    }
